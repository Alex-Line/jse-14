package com.iteco.linealex.jse.api.service;

import org.jetbrains.annotations.NotNull;

public interface ITerminalService {

    @NotNull
    public String nextLine();

}