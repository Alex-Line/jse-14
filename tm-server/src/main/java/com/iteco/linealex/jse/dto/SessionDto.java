package com.iteco.linealex.jse.dto;

import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public final class SessionDto {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull String userId;

    @NotNull Role role;

    @NotNull Date creationDate = new Date(System.currentTimeMillis());

    @NotNull
    public static Session toSession(
            @NotNull final Bootstrap bootstrap,
            @NotNull final SessionDto sessionDto
    ) throws Exception {
        @NotNull final Session session = new Session();
        session.setRole(sessionDto.getRole());
        session.setId(sessionDto.getId());
        session.setCreationDate(sessionDto.getCreationDate());
        session.setUser(bootstrap.getUserService().getEntityById(sessionDto.userId));
        return session;
    }

}