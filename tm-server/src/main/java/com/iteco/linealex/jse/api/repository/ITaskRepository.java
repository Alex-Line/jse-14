package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.entity.Task;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITaskRepository {

    @NotNull
    @ResultType(Task.class)
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId};")
    Collection<Task> findAllByUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    ) throws Exception;

    @Nullable
    @ResultType(Task.class)
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks WHERE taskId = #{taskId};")
    Task findOneByTaskId(
            @NotNull @Param("taskId") final String entityId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks WHERE users_userId = #{userId};")
    Collection<Task> findAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @Nullable
    @ResultType(Task.class)
    @Select("SELECT * FROM TaskManager.tasks WHERE taskName = #{taskName};")
    Task findOneByName(
            @NotNull @Param("taskName") final String taskName
    ) throws Exception;

    @Nullable
    @ResultType(Task.class)
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.task " +
            "WHERE users_userId = #{userId} AND projects_projectId = null AND taskName = #{taskName};")
    Task findOneByNameAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("taskName") final String taskName
    ) throws Exception;

    @Nullable
    @ResultType(Task.class)
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.task " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId} AND taskName = #{taskName};")
    Task findOneByNameAndUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId,
            @NotNull @Param("taskName") final String taskName
    ) throws Exception;

    @Insert("INSERT INTO TaskManager.tasks (taskId, taskName, taskDescription, taskStartDate, " +
            "taskFinishDate, taskStatus, users_userId, projects_projectId) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{status}, #{userId}, #{projectId})")
    void persist(
            @NotNull final Task example
    ) throws Exception;

    @Update("UPDATE TaskManager.tasks " +
            "SET taskName = #{name}, taskDescription = #{description}, " +
            "taskStartDate = #{dateStart}, taskFinishDate = #{dateFinish}, " +
            "taskStatus = #{status}, users_userId = #{userId}, projects_projectId = #{projectId},  " +
            "WHERE taskId = #{id}")
    void merge(
            @NotNull final Task example
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.tasks WHERE taskId = #{taskId};")
    void remove(
            @NotNull @Param("taskId") final String entityId
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.tasks;")
    void removeAll() throws Exception;

    @Delete("DELETE FROM TaskManager.tasks WHERE users_userId = #{userId};")
    void removeAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId};")
    void removeAllByUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks;")
    Collection<Task> findAll() throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId} AND " +
            "AND (taskName LIKE '%#{pattern}%' OR taskDescription LIKE '%#{pattern}%' );")
    Collection<Task> findAllByNameAndUserIdAndProjectId(
            @NotNull @Param("userId") final String pattern,
            @NotNull @Param("projectId") final String projectId,
            @NotNull @Param("pattern") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = null AND " +
            "AND (taskName LIKE '%#{pattern}%' OR taskDescription LIKE '%#{pattern}%' );")
    Collection<Task> findAllByNameAndUserId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("pattern") final String pattern
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "AND (taskName LIKE '%#{pattern}%' OR taskDescription LIKE '%#{pattern}%' );")
    Collection<Task> findAllByName(
            @NotNull @Param("pattern") final String pattern
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId} " +
            "ORDER BY taskStartDate;")
    Collection<Task> findAllSortedByStartDateAndUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = null " +
            "ORDER BY projectStartDate;")
    Collection<Task> findAllSortedByStartDateAndUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "ORDER BY taskStartDate;")
    Collection<Task> findAllSortedByStartDate() throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId} " +
            "ORDER BY taskFinishDate;")
    Collection<Task> findAllSortedByFinishDateAndUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = null " +
            "ORDER BY taskFinishDate;")
    Collection<Task> findAllSortedByFinishDateAndUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks ORDER BY taskFinishDate;")
    Collection<Task> findAllSortedByFinishDate() throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = #{projectId}" +
            "ORDER BY FIELD(taskStatus, 'PLANNED', 'PROCESSING', 'DONE');")
    Collection<Task> findAllSortedByStatusAndUserIdAndProjectId(
            @NotNull @Param("userId") final String userId,
            @NotNull @Param("projectId") final String projectId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "WHERE users_userId = #{userId} AND projects_projectId = null " +
            "ORDER BY FIELD(taskStatus, 'PLANNED', 'PROCESSING', 'DONE');")
    Collection<Task> findAllSortedByStatusAndUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

    @NotNull
    @Results({
            @Result(property = "id", column = "taskId", id = true),
            @Result(property = "name", column = "taskName"),
            @Result(property = "description", column = "taskDescription"),
            @Result(property = "dateStart", column = "taskStartDate"),
            @Result(property = "dateFinish", column = "taskFinishDate"),
            @Result(property = "userId", column = "users_userId"),
            @Result(property = "projectId", column = "projects_projectId"),
            @Result(property = "status", column = "taskStatus")})
    @Select("SELECT * FROM TaskManager.tasks " +
            "ORDER BY FIELD(taskStatus, 'PLANNED', 'PROCESSING', 'DONE');")
    Collection<Task> findAllSortedByStatus() throws Exception;

}