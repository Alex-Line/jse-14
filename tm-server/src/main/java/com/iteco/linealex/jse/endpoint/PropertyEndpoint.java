package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.IPropertyEndpoint;
import com.iteco.linealex.jse.context.Bootstrap;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.IPropertyEndpoint")
public class PropertyEndpoint extends AbstractEndpoint implements IPropertyEndpoint {

    public PropertyEndpoint(
            @NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Nullable
    @Override
    @WebMethod
    public String getProperty(
            @WebParam(name = "propertyName", partName = "propertyName") @Nullable final String propertyName) {
        return getBootstrap().getPropertyService().getProperty(propertyName);
    }

}