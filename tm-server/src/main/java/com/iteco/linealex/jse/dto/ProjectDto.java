package com.iteco.linealex.jse.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDto {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    String name;

    @Nullable
    String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DDThh:mm:ss±hh:mm")
    Date dateFinish;

    @Nullable
    String userId;

    @NotNull
    Status status;

    @Nullable
    public static Project toProject(
            @NotNull final Bootstrap bootstrap,
            @NotNull final ProjectDto projectDto
    ) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(projectDto.getId());
        @Nullable final User user = bootstrap.getUserService().getEntityById(projectDto.getUserId());
        if (user == null) return null;
        project.setUser(user);
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());
        project.setStatus(projectDto.getStatus());
        project.setDateStart(projectDto.getDateStart());
        project.setDateFinish(projectDto.getDateFinish());
        return project;
    }

}