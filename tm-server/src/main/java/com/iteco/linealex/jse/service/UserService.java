package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.api.service.IUserService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.user.*;
import com.iteco.linealex.jse.repository.UserRepository;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @Nullable
    @Override
    public User getEntityById(@Nullable final String entityId) {
        if (entityId == null) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final User user = new UserRepository(entityManager).findOne(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @NotNull
    @Override
    public Collection<User> getAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Collection<User> users = new UserRepository(entityManager).findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return users;
    }

    @Nullable
    @Override
    public User logInUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        if (password == null || password.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashPassword = TransformatorToHashMD5.getHash(password,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final User user = new UserRepository(entityManager).findOneByName(login, hashPassword);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Override
    public void createUser(
            @Nullable final User user,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (user == null) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@Nullable final User entity) {
        if (entity == null) return;
        if (entity.getLogin().isEmpty()) return;
        if (entity.getHashPassword() == null || entity.getHashPassword().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).persist(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@NotNull Collection<User> collection) throws Exception {
        if (collection.isEmpty()) return;
        for (@Nullable final User user : collection) {
            if (user == null) continue;
            persist(user);
        }
    }

    @Override
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).remove(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities(@Nullable final String userId) {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final User entity) {
        if (entity == null) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).merge(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (oldPassword == null || oldPassword.isEmpty()) throw new WrongPasswordException();
        @NotNull final String hashOldPassword = TransformatorToHashMD5.getHash(oldPassword,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        if (selectedUser.getHashPassword() == null
                && !selectedUser.getHashPassword().equals(hashOldPassword)) throw new WrongPasswordException();
        if (newPassword == null || newPassword.isEmpty()) throw new WrongPasswordException();
        if (newPassword.length() < 8) throw new ShortPasswordException();
        @NotNull final String hashNewPassword = TransformatorToHashMD5.getHash(newPassword,
                getPropertyService().getProperty("PASSWORD_SALT"),
                Integer.parseInt(getPropertyService().getProperty("PASSWORD_TIMES")));
        selectedUser.setHashPassword(hashNewPassword);
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new UserRepository(entityManager).merge(selectedUser);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    public User getUser(
            @Nullable final String login,
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) throw new UserIsNotLogInException();
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        if (login == null || login.isEmpty()) throw new LoginIncorrectException();
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final User user = new UserRepository(entityManager)
                .findOneByName(login, selectedUser.getHashPassword());
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @NotNull
    public Collection<User> getAllUsers(
            @Nullable final User selectedUser
    ) throws Exception {
        if (selectedUser == null) return Collections.EMPTY_LIST;
        if (selectedUser.getRole() != Role.ADMINISTRATOR) throw new LowAccessLevelException();
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<User> user = new UserRepository(entityManager).findAll(selectedUser.getId());
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

}