package com.iteco.linealex.jse.endpoint;

import com.iteco.linealex.jse.api.endpoint.ISessionEndpoint;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.dto.SessionDto;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.exception.user.UserIsNotExistException;
import com.iteco.linealex.jse.util.ApplicationUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Date;

@WebService(endpointInterface = "com.iteco.linealex.jse.api.endpoint.ISessionEndpoint")
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(
            @NotNull final Bootstrap bootstrap
    ) {
        super(bootstrap);
    }

    @Override
    @WebMethod
    public SessionDto createSession(
            @WebParam(name = "login", partName = "login") @NotNull final String login,
            @WebParam(name = "password", partName = "password") @NotNull final String password
    ) throws Exception {
        @NotNull final Session session = new Session();
        @Nullable final User user = getBootstrap().getUserService().logInUser(login, password);
        if (user == null) throw new UserIsNotExistException();
        session.setUser(user);
        session.setRole(user.getRole());
        session.setCreationDate(new Date());
        session.setSignature(ApplicationUtils.getSignature(Session.toSessionDto(session),
                getBootstrap().getPropertyService()));
        getBootstrap().getSessionService().persist(session);
        return Session.toSessionDto(session);
    }

    @Override
    @WebMethod
    public void removeSession(
            @WebParam(name = "sessionId", partName = "sessionId") @NotNull final String sessionId
    ) throws Exception {
        getBootstrap().getSessionService().removeEntity(sessionId);
    }

    @Nullable
    @Override
    @WebMethod
    public SessionDto findSession(
            @WebParam(name = "sessionId", partName = "sessionId") @Nullable final String sessionId
    ) throws Exception {
        if (sessionId == null) return null;
        @Nullable final Session session = getBootstrap().getSessionService().getEntityById(sessionId);
        if (session == null) return null;
        return Session.toSessionDto(session);
    }

}