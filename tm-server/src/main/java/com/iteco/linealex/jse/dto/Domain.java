package com.iteco.linealex.jse.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.api.service.ISessionService;
import com.iteco.linealex.jse.api.service.ITaskService;
import com.iteco.linealex.jse.api.service.IUserService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"users", "projects", "tasks", "sessions"}, name = "Domain")
public class Domain implements Serializable {

    public static final long serialVersionUID = 1L;

    @Nullable
    @XmlElement(name = "user")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "users")
    private List<User> users;

    @Nullable
    @XmlElement(name = "project")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "projects")
    private List<Project> projects;

    @Nullable
    @XmlElement(name = "task")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "tasks")
    private List<Task> tasks;

    @Nullable
    @XmlElement(name = "session")
    @JsonIgnoreProperties(ignoreUnknown = true)
    @XmlElementWrapper(required = true, name = "sessions")
    private List<Session> sessions;

    public void load(
            @NotNull final Bootstrap bootstrap
    ) throws Exception {
        users = new LinkedList<>(bootstrap.getUserService().getAllEntities());
        projects = new LinkedList<>(bootstrap.getProjectService().getAllEntities());
        tasks = new LinkedList<>(bootstrap.getTaskService().getAllEntities());
        sessions = new LinkedList<>(bootstrap.getSessionService().getAllEntities());
    }

    public void load(
            @NotNull final IUserService userService,
            @NotNull final IProjectService projectService,
            @NotNull final ITaskService taskService,
            @NotNull final ISessionService sessionService
    ) throws Exception {
        users = new LinkedList<>(userService.getAllEntities());
        projects = new LinkedList<>(projectService.getAllEntities());
        tasks = new LinkedList<>(taskService.getAllEntities());
        sessions = new LinkedList<>(sessionService.getAllEntities());
    }

}