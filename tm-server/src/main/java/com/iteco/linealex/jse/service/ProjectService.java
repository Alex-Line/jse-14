package com.iteco.linealex.jse.service;

import com.iteco.linealex.jse.api.service.IProjectService;
import com.iteco.linealex.jse.api.service.IPropertyService;
import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;

public final class ProjectService extends AbstractTMService<Project> implements IProjectService {

    public ProjectService(
            @NotNull final IPropertyService propertyService,
            @NotNull final Bootstrap bootstrap
    ) {
        super(propertyService, bootstrap);
    }

    @Nullable
    @Override
    public Project getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Project project = new ProjectRepository(entityManager).findOne(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager).findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @Override
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        if (project.getName() == null || project.getName().isEmpty()) return;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new ProjectRepository(entityManager).persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@NotNull Collection<Project> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final Project project : collection) {
            if (project == null) continue;
            persist(project);
        }
    }

    @Override
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new ProjectRepository(entityManager).remove(entityId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new ProjectRepository(entityManager).removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllEntities() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new ProjectRepository(entityManager).removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Project entity) {
        if (entity == null) return;
        if (entity.getName() == null || entity.getName().isEmpty()) return;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        new ProjectRepository(entityManager).merge(entity);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Project getEntityByName(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Project project = new ProjectRepository(entityManager).findOneByName(entityName);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @Nullable final Project project = new ProjectRepository(entityManager).findOneByName(userId, entityName);
        entityManager.getTransaction().commit();
        entityManager.close();
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllByName(userId, pattern);
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllByName(pattern);
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllSortedByStartDate();
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllSortedByStartDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllSortedByFinishDate();
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllSortedByFinishDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus() {
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllSortedByStatus();
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAllSortedByStatus(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final EntityManager entityManager = getBootstrap().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final Collection<Project> collection = new ProjectRepository(entityManager)
                .findAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return collection;
    }

}