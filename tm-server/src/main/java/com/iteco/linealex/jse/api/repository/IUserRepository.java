package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.entity.User;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface IUserRepository {

    @NotNull
    @ResultType(User.class)
    @Results({
            @Result(property = "id", column = "userId", id = true),
            @Result(property = "login", column = "userLogin"),
            @Result(property = "hashPassword", column = "userHashPassword"),
            @Result(property = "role", column = "userRole")
    })
    @Select("SELECT * FROM TaskManager.users;")
    Collection<User> findAll() throws Exception;

    @Nullable
    @Results({
            @Result(property = "id", column = "userId", id = true),
            @Result(property = "login", column = "userLogin"),
            @Result(property = "hashPassword", column = "userHashPassword"),
            @Result(property = "role", column = "userRole")
    })
    @Select("SELECT * FROM TaskManager.users " +
            "WHERE userLogin = #{login} AND userHashPassword = #{hashPassword};")
    User findOneByLoginAndPassword(
            @NotNull @Param("login") final String login,
            @NotNull @Param("hashPassword") final String hashPassword
    ) throws Exception;

    @Nullable
    @ResultType(User.class)
    @Results(id = "userResult", value = {
            @Result(property = "id", column = "userId", id = true),
            @Result(property = "login", column = "userLogin"),
            @Result(property = "hashPassword", column = "userHashPassword"),
            @Result(property = "role", column = "userRole")
    })
    @Select("SELECT * FROM TaskManager.users " +
            "WHERE userLogin = #{login};")
    User findOneByLogin(
            @NotNull @Param("login") final String login
    ) throws Exception;

    @Insert("INSERT INTO TaskManager.users (userId, userLogin, userHashPassword, userRole) " +
            "VALUES (#{id}, #{login}, #{hashPassword}, #{role})")
    void persist(
            @NotNull final User example
    ) throws Exception;

    @Nullable
    @Results({
            @Result(property = "id", column = "userId", id = true),
            @Result(property = "login", column = "userLogin"),
            @Result(property = "hashPassword", column = "userHashPassword"),
            @Result(property = "role", column = "userRole")
    })
    @Select("SELECT * FROM TaskManager.users " +
            "WHERE userId = #{userId};")
    User findOneByUserId(
            @NotNull @Param("userId") final String entityId
    ) throws Exception;

    @Update("UPDATE `TaskManager`.`users` " +
            "SET `userHashPassword` = #{hashPassword}, userLogin = #{login} " +
            "WHERE (`userId` = #{id});")
    void merge(
            @NotNull final User example
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.users " +
            "WHERE userId = #{userId};")
    void remove(
            @NotNull @Param("userId") final String entityId
    ) throws Exception;

    @Delete("DELETE FROM TaskManager.users;")
    void removeAll() throws Exception;

    @Delete("DELETE FROM TaskManager.users " +
            "WHERE userId = #{userId};")
    void removeAllByUserId(
            @NotNull @Param("userId") final String userId
    ) throws Exception;

}