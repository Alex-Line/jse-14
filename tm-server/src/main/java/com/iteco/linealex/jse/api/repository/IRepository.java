package com.iteco.linealex.jse.api.repository;

import com.iteco.linealex.jse.exception.entity.InsertExistingEntityException;
import org.jetbrains.annotations.NotNull;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Collection;

public interface IRepository<T> {

    public boolean contains(@NotNull final String entityId) throws SQLException;

    @NotNull
    public Collection<T> findAll() throws SQLException;

    public void persist(@NotNull final T example) throws InsertExistingEntityException, NoSuchAlgorithmException, SQLException;

    public void persist(@NotNull final Collection<T> collection) throws SQLException;

    public void merge(@NotNull final T example) throws InsertExistingEntityException, SQLException;

    public void remove(@NotNull final String name) throws SQLException;

    public void removeAll() throws SQLException;

}