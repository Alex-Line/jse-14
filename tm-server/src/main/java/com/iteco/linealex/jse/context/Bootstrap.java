package com.iteco.linealex.jse.context;

import com.iteco.linealex.jse.api.endpoint.*;
import com.iteco.linealex.jse.api.service.*;
import com.iteco.linealex.jse.endpoint.*;
import com.iteco.linealex.jse.entity.Project;
import com.iteco.linealex.jse.entity.Session;
import com.iteco.linealex.jse.entity.Task;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import com.iteco.linealex.jse.exception.TaskManagerException;
import com.iteco.linealex.jse.service.*;
import com.iteco.linealex.jse.util.TransformatorToHashMD5;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.HashMap;
import java.util.Map;

@Getter
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectService projectService = new ProjectService(propertyService, this);

    @NotNull
    private final ITaskService taskService = new TaskService(propertyService, this);

    @NotNull
    private final IUserService userService = new UserService(propertyService, this);

    @NotNull
    private final ISessionService sessionService = new SessionService(propertyService, this);

    @NotNull
    private final IProjectEndpoint projectEndpoint
            = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint
            = new UserEndpoint(this);

    @NotNull
    final IPropertyEndpoint propertyEndpoint = new PropertyEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @NotNull
    private EntityManagerFactory entityManagerFactory;

    public EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    public EntityManager getEntityManager() {
        return this.entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory createEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyEndpoint.getProperty("DB_DRIVER"));
        settings.put(Environment.URL, propertyService.getProperty("DB_URL"));
        settings.put(Environment.USER, propertyService.getProperty("DB_USER"));
        settings.put(Environment.PASS, propertyService.getProperty("DB_PASSWORD"));
        settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources metadataSources = new MetadataSources(registry);
        metadataSources.addAnnotatedClass(User.class);
        metadataSources.addAnnotatedClass(Session.class);
        metadataSources.addAnnotatedClass(Project.class);
        metadataSources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = metadataSources.getMetadataBuilder().build();
        return metadata.buildSessionFactory();
    }

    public void start() throws Exception {
        setEntityManagerFactory(createEntityManagerFactory());
        taskService.removeAllEntities();
        projectService.removeAllEntities();
        sessionService.removeAllEntities();
        userService.removeAllEntities();
        createInitialUsers();
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
        Endpoint.publish("http://localhost:8080/propertyService?wsdl", propertyEndpoint);
    }

    private void createInitialUsers() {
        try {
            @NotNull final User admin = new User();
            admin.setLogin(propertyEndpoint.getProperty("ADMIN"));
            admin.setHashPassword(TransformatorToHashMD5.getHash(
                    propertyEndpoint.getProperty("ADMIN_PASSWORD"),
                    propertyEndpoint.getProperty("PASSWORD_SALT"),
                    Integer.parseInt(propertyEndpoint.getProperty("PASSWORD_TIMES"))));
            admin.setRole(Role.ADMINISTRATOR);
            userService.persist(admin);
            @NotNull final User user = new User();
            user.setLogin(propertyEndpoint.getProperty("USER"));
            user.setHashPassword(TransformatorToHashMD5.getHash(
                    propertyEndpoint.getProperty("USER_PASSWORD"),
                    propertyEndpoint.getProperty("PASSWORD_SALT"),
                    Integer.parseInt(propertyEndpoint.getProperty("PASSWORD_TIMES"))));
            userService.persist(user);
        } catch (TaskManagerException e) {
            System.out.println(e.getMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}