package com.iteco.linealex.jse.dto;

import com.iteco.linealex.jse.context.Bootstrap;
import com.iteco.linealex.jse.entity.User;
import com.iteco.linealex.jse.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class UserDto {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String login = "unnamed";

    @Nullable
    private String hashPassword;

    @NotNull
    private Role role = Role.ORDINARY_USER;

    @NotNull
    public static User toUser(
            @NotNull final Bootstrap bootstrap,
            @NotNull final UserDto userDto
    ) throws Exception {
        @NotNull final User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setHashPassword(userDto.getHashPassword());
        user.setRole(userDto.getRole());
        user.setProjects(new ArrayList<>(bootstrap.getProjectService().getAllEntities(userDto.getId())));
        user.setTasks(new ArrayList<>(bootstrap.getTaskService().getAllEntities(userDto.getId())));
        return user;
    }

}