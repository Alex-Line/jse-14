package com.iteco.linealex.jse.command.data.load;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataLoadJaxBFromXmlCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-jaxb-xml";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD DATA FROM XML FILE BY JAXB";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("LOADING DATA FROM XML FILE");
        serviceLocator.getUserEndpoint().loadJaxbXml(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}