package com.iteco.linealex.jse.command.data.load;

import com.iteco.linealex.jse.api.endpoint.TaskManagerException_Exception;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataLoadBinaryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-load-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "LOAD ALL DATA FROM BINARY FILE THE CLASSIC JAVA WAY";
    }

    @Override
    public void execute() throws TaskManagerException_Exception, Exception {
        System.out.println("LOADING DATA FROM BINARY FILE");
        serviceLocator.getUserEndpoint().loadBinary(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}