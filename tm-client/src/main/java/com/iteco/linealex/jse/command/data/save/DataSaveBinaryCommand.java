package com.iteco.linealex.jse.command.data.save;

import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

public class DataSaveBinaryCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data-save-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "SAVE ALL DATA INTO BINARY FILE THE CLASSIC JAVA WAY";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("SAVING DATA TO BINARY FILE");
        serviceLocator.getUserEndpoint().saveBinary(serviceLocator.getSession());
        System.out.println("[OK]\n");
    }

    @Override
    public boolean secure() {
        return false;
    }

}