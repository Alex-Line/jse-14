package com.iteco.linealex.jse.command.project;

import com.iteco.linealex.jse.api.endpoint.ProjectDto;
import com.iteco.linealex.jse.api.endpoint.Role;
import com.iteco.linealex.jse.api.endpoint.SessionDto;
import com.iteco.linealex.jse.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;

public final class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "SHOW ALL PROJECTS";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDto session = serviceLocator.getSession();
        System.out.println("[PROJECT LIST]");
        @NotNull Collection<ProjectDto> collection = Collections.EMPTY_LIST;
        if (session.getRole() == Role.ADMINISTRATOR) {
            collection = serviceLocator.getProjectEndpoint().getAllProjects(session);
        } else collection = serviceLocator.getProjectEndpoint()
                .getAllProjectsWithUserId(session, session.getUserId());
        if (collection.isEmpty()) {
            System.out.println("[THERE IS NOT ANY PROJECTS]\n");
            return;
        }
        int index = 1;
        for (@NotNull final ProjectDto project : collection) {
            System.out.println(index + ". " + "Project: " + project.getName() +
                    " { ID = " + project.getId() +
                    ",\n    description = '" + project.getDescription() + '\'' +
                    ",\n    Start Date = " + project.getDateStart() +
                    ",\n    Finish Date = " + project.getDateFinish() +
                    ",\n    Status = " + project.getStatus().name() +
                    ",\n    User ID = " + project.getUserId() +
                    " }");
            index++;
        }
        System.out.println();
    }

    @Override
    public boolean secure() {
        return true;
    }

}